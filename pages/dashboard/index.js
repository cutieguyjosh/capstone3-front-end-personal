import React, { useContext, useEffect, useState } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import { Container, Col, Row, Form, Button, Table } from 'react-bootstrap';
import UserContext from '../../UserContext';
import toNum from '../toNum';
import Balance from '../../components/Balance';
import moment from 'moment';

export default function index() {

	const [transaction, setTransactions] = useState()
	const [income, setIncome] = useState()
	const [expense, setExpense] = useState()
	const [totalIncome, setTotalIncome] = useState()
	const [totalExpense, setTotalExpense] = useState()
	const [search, setSearch] = useState("")
	const [allRecord, setAllRecord] = useState([])
	const [matchSearch, setMatchSearch] = useState()
	const [filterType, setFilterType] = useState('All')

	useEffect (() => {

		const table = async () => {

			const options = {
				headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
			}

			const fetchData = await fetch('https://aqueous-hamlet-35125.herokuapp.com/api/users/details', options)
			const data = await fetchData.json()

			const array = []

			const tempRecords = data.transaction.filter(records => {
				if(filterType === 'All') {
					return true
				} else if(filterType === records.transaction_type ) {
					return true
				}
			})

			tempRecords.map(records => {
				return array.push(
					<tr>
						<td>{ moment(records.createdAt).format('LL') }</td>
						<td>{ records.category_name }</td>
						<td>{ records.transaction_type }</td>
						<td>{ records.description }</td>
						<td>₱{ records.amount }</td>
						<td>₱{ records.currentBalance }</td>
					</tr>
				)
			})

			setTransactions(array)
			setAllRecord(data.transaction)
		}
			table()

	}, [filterType])

	function searchRecord() {

		setSearch("")

		const match = allRecord.find(record => {
			
			return search === record.description 
		})
		console.log(match)

		const searchItem = 
		<tr>
			<td>{ moment(match.createdAt).format('LL') }</td>
			<td>{ match.category_name }</td>
			<td>{ match.transaction_type }</td>
			<td>{ match.description }</td>
			<td>₱{ match.amount }</td>
			<td>₱{ match.currentBalance }</td>
		</tr>
		console.log(searchItem)


		setMatchSearch(searchItem)
	}


	return (
		<React.Fragment>
			<Head>
				<title>Dashboard</title>
			</Head>
			<Container>
				<h2 className="mt-3">Budgetarian Dashboard</h2>
				<br />
				<div className="mb-5">
					<Form className="dashboard-flex">
						<Link href="/newTransaction">
						<Button variant="success">Add</Button>
						</Link>

							<Form.Label></Form.Label>
							<Form.Control placeholder="Search Record" value={search} onChange={(e) => setSearch(e.target.value)} />
							<Button onClick={searchRecord}> Search </Button>

							<Form.Control as="select" value={filterType} onChange={(e) => setFilterType(e.target.value)}>
								<option value="All">All</option>
								<option value="Income">Income</option>
								<option value="Expense">Expense</option>
							</Form.Control>
					</Form>
				</div>
				<div>
					<h4>Transaction History</h4>
					<Table striped bordered hover>
						<thead>
							<tr>
								<th>Date</th>
								<th>Category Name</th>
								<th>Category Type</th>
								<th>Description</th>
								<th>Amount</th>
								<th>Current Balance</th>
							</tr>
						</thead>
						<tbody>
							{ matchSearch ? matchSearch : transaction  }
						</tbody>
					</Table>
				</div>	
			</Container>
			<Balance />
		</React.Fragment>
	)

} 

