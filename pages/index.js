import React from 'react';
import Head from 'next/head';
import PageHeader from '../components/PageHeader';
import styles from '../styles/Home.module.css';
import NavBar from '../components/NavBar';
import Register from './register/index.js';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Login from './login/index.js';


export default function Home() {

	return (
	<React.Fragment>
		<PageHeader pagetitle={"Home Page"} />
	  	<Login />
	</React.Fragment>
	)
}
