import React, { useEffect, useState } from 'react';
import View from '../../components/View';
import Head from 'next/head';
import Link from 'next/link';
import { Col, Row, Table, Container } from 'react-bootstrap';
import toNum from '../toNum';
import { Doughnut, Line } from 'react-chartjs-2';

export default function index() {

	const [income, setIncome] = useState()
	const [expense, setExpense] = useState()
	const [totalIncome, setTotalIncome] = useState()
	const [totalExpense, setTotalExpense] = useState()


	useEffect (() => {

		const allStatus = async () => {

			const options = {
				headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
			}

			const balanceStatus = await fetch('https://aqueous-hamlet-35125.herokuapp.com/api/users/details', options)

			const data = await balanceStatus.json()

			let incomeGroup = []
			let expenseGroup = []
			let income = 0
			let expense = 0

			data.transaction.map(computation => {
				if(computation.transaction_type === 'Income') {
					incomeGroup.push(computation.amount)
					income += toNum(computation.amount)
				}
				else if(computation.transaction_type === 'Expense') {
					expenseGroup.push(computation.amount)
					expense += toNum(computation.amount)
				}

			})

			setIncome(incomeGroup)
			setExpense(expenseGroup)
			setTotalIncome(income)
			setTotalExpense(expense)

		}

			allStatus();

	}, [])

	


	return(
		<React.Fragment>
			<View title={ 'Analaytics' }>
			<Head>
				<title>Analaytics</title>
			</Head>
			</View>
			<Container>
			<div className="mb-5">
			<h2>Analytics</h2>
			</div>
			<div>
			<h6> Transaction Type Breakdown</h6>
			<Doughnut
			            data={{
			              datasets: [
			                {
			                  data: [totalIncome, totalExpense],
			                  backgroundColor: ['green', 'red'],
			                },
			              ],
			              labels: ['Income', 'Expenses'],
			            }}
			          />
			</div>

			<div className="mt-4">
			<h6>Budget Trend</h6>
          	<Line
          	          options={{
          	            responsive: true,
          	          }}
          	          data={{
          	            datasets: [
          	              {
          	                backgroundColor: 'rgba(0, 255, 0, 0.65)',
          	                label: 'Income',
          	                data: income,
          	              },
          	              {
          	                backgroundColor: 'rgba(128, 0, 0, 0.65)',
          	                label: 'Expense',
          	                data: expense,
          	              },
          	            ],
          	            labels: ['1', '2', '3', '4', '5', '6'],
          	          }}
          	        />
  	        </div>
  	        </Container>
		</React.Fragment>
		
	)

}




