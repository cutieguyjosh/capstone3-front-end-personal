import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import Router from 'next/router';
import { Form, Button, Container } from 'react-bootstrap';
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2'


export default function index() {

	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [email, setEmail] = useState("")
	const [password1, setPassword1] = useState("")
	const [password2, setPassword2] = useState("")
	const [isActive, setIsActive] = useState(false)

	function registerUser(e) {
		e.preventDefault()

		const options = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email
			})
		}

		const options2 = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password1
			})
		}

		fetch('https://aqueous-hamlet-35125.herokuapp.com/api/users/email-exists', options)
		.then(AppHelper.toJSON)
		.then(data => {
			if(data === false) {

				fetch('https://aqueous-hamlet-35125.herokuapp.com/api/users/', options2)
				.then(AppHelper.toJSON)
				.then(data => {
					if(data === true) {
						Swal.fire("Good job!", "Registered Successfully", "success")
					}
				})
			} else {
				Swal.fire("Uh-oh!", "Email already exist.", "error")
			}
		})

		Router.push('/login')


		// console.log(`Registration successful. User ${email} registered with password ${password1}`)


		setFirstName("")
		setLastName("")
		setEmail("")
		setPassword1("")
		setPassword2("")
	}

	useEffect(() => {
		//validation to enable submit button when all fields are populated and passwords match
		if((firstName !== "" && lastName !== "" && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, email, password1, password2])


	return(
		<React.Fragment>
			<Head>
				<title>Registration Page</title>
			</Head>
			<Container>
				<Form onSubmit={e => registerUser(e)} className="col-lg-4 offset-lg-4 my-5">
					<Form.Group>
						<Form.Label>First Name</Form.Label>
						<Form.Control type="name" placeholder="Enter First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required />
					</Form.Group>

					<Form.Group>
						<Form.Label>Last Name</Form.Label>
						<Form.Control type="name" placeholder="Enter Last Name" value={lastName} onChange={e => setLastName(e.target.value)} required  />
					</Form.Group>

					<Form.Group controlId="email">
						<Form.Label>E-Mail</Form.Label>
						<Form.Control type="email" placeholder="Enter E-Mail Address" value={email} onChange={e => setEmail(e.target.value)} required  />
					</Form.Group>

					<Form.Group controlId="password1">
						<Form.Label>Password</Form.Label>
						<Form.Control type="password" placeholder="Please enter your password" value={password1} onChange={e => setPassword1(e.target.value)} />
					</Form.Group>

					<Form.Group controlId="password2">
						<Form.Label>Verify Password</Form.Label>
						<Form.Control type="password" placeholder="Please verify your password" value={password2} onChange={e => setPassword2(e.target.value)} />
					</Form.Group>
					{isActive 
						?
							<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
						:
							<Button variant="secondary" type="submit" id="submitBtn" disabled>Submit</Button>
					}
				</Form>
			</Container>
		</React.Fragment>
	)

}