import React, { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { UserProvider } from '../UserContext.js';
import AppHelper from '../app-helper';
import NavBar from '../components/NavBar';

export default function MyApp({ Component, pageProps }) {
 
	const [user, setUser] = useState({
		id: null
	})

	const unsetUser = () => {
		localStorage.clear();

		setUser({
			is:null
		})
	}

	useEffect(() => {

		const options = {
			headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
		}

		fetch('https://aqueous-hamlet-35125.herokuapp.com/api/users/details', options)
		.then(AppHelper.toJSON)
		.then(data => {
			if(typeof data._id !== undefined) {
				setUser({ id: data._id })
			} else {
				setUser({ id: null })
			}
		})
	}, [user.id])

	useEffect(() => {
		// console.log(`User with id: ${user._id}`);
	}, [ user.id ])	


	return (
		<React.Fragment>
			<UserProvider value={{ user, setUser, unsetUser }}>
				<NavBar />
				<Container>
					<Component {...pageProps} />
				</Container>
			</UserProvider>
		</React.Fragment>
	)
}