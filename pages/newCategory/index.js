import React, { useContext, useState, useEffect } from 'react';
import Router from 'next/router';
import Head from 'next/head';
import { Container, Col, Row, Form, Button, Card } from 'react-bootstrap';
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2';

export default function index() {

	const [categoryName, setCategoryName] = useState("")
	const [categoryType, setCategoryType] = useState("")

	function addCategory(e) {
		e.preventDefault()

		const options = {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				category_name: categoryName,
				category_type: categoryType
			})
		}

		fetch('https://aqueous-hamlet-35125.herokuapp.com/api/users/category', options)
		.then(AppHelper.toJSON)
		.then(data => {
			if(data === true) {
				Swal.fire("Good job!", "Category Added", "success")
			} else {
				alertSwal.fire("Uh-oh!", "Please try again", "Error")
			}

			setCategoryName("")
			setCategoryType("")

			Router.push('/dashboard')
		}, [categoryType, categoryName])

	}

	return (
		<React.Fragment>
			<Container>
				<Head>
					<title>Add Category</title>
				</Head>
				<h2 className="mt-4">New Category</h2>
				<br />
				<div className="mt-5 pt-4 mb-5">
				<div className="justify-content-center row">
				<div className="col-md-6 col">
					<Card>
						<Form onSubmit={e => addCategory(e)}>
							<Form.Group className="mt-4 mb-5">
								<Form.Label><h4>Category Information</h4></Form.Label>
							</Form.Group>

							<Form.Group>
								<Form.Label>Category Name:</Form.Label>
								<Form.Control placeholder="Enter Category name" onChange={(e) => setCategoryName(e.target.value)}/>
							</Form.Group>
							
							<Form.Group>
								<Form.Label>Category Type:</Form.Label>
								<Form.Control as="select" onChange={(e) => setCategoryType(e.target.value)}>
								<option>Select One:</option>
								<option value="Income">Income</option>
								<option value="Expense">Expense</option>
								</Form.Control>
							</Form.Group>
							
							<Button variant="primary" type="submit" id="submitBtn">Submit</Button>					
						</Form>
					</Card>
				</div>
				</div>
				</div>
			</Container>
		</React.Fragment>
	)
}



