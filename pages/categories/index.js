import React, { useContext, useState, useEffect } from 'react';
import Link from 'next/link';
import { Container, Col, Row, Form, Button, Card, Table } from 'react-bootstrap';
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper';

export default function index() {


	const { user, setUser } = useContext(UserContext);

	const retrieveUserDetails = (accessToken) => {

		const options = {
			headers: { Authorization: `Bearer ${ accessToken }` }
		}

		fetch(`https://aqueous-hamlet-35125.herokuapp.com/users/details`, options)
		.then(AppHelper.toJSON)
		.then(data => {

			console.log(data)

		})
	}


	return(
		<React.Fragment>
			<h2>Categories</h2>
				<Link href="/newCategory">
				<Button variant="success" className="mt-2">Add</Button>
				</Link>
				<Table stiped bordered hover className="mt-3">
					<thead>
						<tr>
							<th>Category</th>
							<th>Type</th>
						</tr>
					</thead>
				</Table>

		</React.Fragment>
	)

}
