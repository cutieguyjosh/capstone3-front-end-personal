import React, { useState, useEffect, useContext } from 'react';
import Link from 'next/link';
import ReactDOM from 'react-dom';
import Router from 'next/router';
import Head from 'next/head';
import { Container, Button, Form, Row, Col } from 'react-bootstrap';
import { GoogleLogin } from 'react-google-login';
import PageHeader from '../../components/PageHeader';
import Swal from 'sweetalert2'
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper';
import View from '../../components/View';
import Highlights from '../../components/Highlights';

export default function index() {

	return (
		<View title={ 'Login' }>
			<Row className="justify-content-center">
				<Col xs md="6">
					<h3>Login</h3>
					<LoginForm />
				</Col>
			</Row>
		</View>
	)
}

const LoginForm = () => {

	// Consume UserContext and destructure it to access the user and setUser defined in the App component
	const { user, setUser } = useContext(UserContext);
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	function authenticate(e) {

		e.preventDefault();

		const options = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password
			})
		}

		fetch('https://aqueous-hamlet-35125.herokuapp.com/api/users/login', options)
		.then(AppHelper.toJSON)
		.then(data => {

			console.log(data);

			if(typeof data.accessToken !== 'undefined') {
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);
				// Router.push('/dashboard');
			} else {

				if(data.error === 'incorrect-password') {
					Swal.fire('Authentication Failed', 'Password is incorrect', 'error')
				} else if (data.error === 'does-not-exist') {
					Swal.fire('Authentication Failed', 'User does not exist', 'error')
				} else if (data.error === 'login-type-error') {
					Swal.fire('Login Type Error', 'You may have registeres through a different login procedure, try alternative login procedures', 'error')
				}
			}
		})

	}
	// console.log(user)

	const authenticateGoogleToken = (response) => {

		console.log(response);

		const options = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({ tokenId: response.tokenId })
		}

		fetch('https://aqueous-hamlet-35125.herokuapp.com/api/users/verify-google-id-token', options)
		.then(AppHelper.toJSON)
		.then(data => {

			if(typeof data.accessToken !== 'undefined') {
				localStorage.setItem('token', data.accessToken)
				retrieveUserDetails(data.accessToken)
			} else {
				if (data.error === 'google-auth-error') {
					Swal.fire('Google Auth Error', 'Google authentication procedure failed', 'error')
				} else if (data.error === 'login-type-error') {
					Swal.fire('Login Type Error', 'You may have registered through a different login procedure', 'error')
				}
			}
		})
	}


	const retrieveUserDetails = (accessToken) => {

		const options = {
			headers: { Authorization: `Bearer ${ accessToken }` }
		}

		fetch('https://aqueous-hamlet-35125.herokuapp.com/api/users/details', options)
		.then(AppHelper.toJSON)
		.then(data => {


			setUser({ id: data._id })
			Router.push("/dashboard")
		})
	}


	return (
		<React.Fragment>
			<Head>
				<title>Login Authentication</title>
			</Head>
			<Container>
				<Form onSubmit={e => authenticate(e)}>

					<Form.Group controlId="email">
						<Form.Label> Email: </Form.Label>
							<Form.Control type="email" placeholder="E-Mail Address" value={email} onChange={e => setEmail(e.target.value)}  required/>
					</Form.Group>

					<Form.Group controlId="password">
						<Form.Label> Password: </Form.Label>
							<Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} required/>
					</Form.Group>

					<Button variant="primary" type="submit" block>Login</Button>

					<GoogleLogin 
						clientId="1090663674297-k8l4gku842tdu3048jptct64f47ej7o2.apps.googleusercontent.com"
						buttonText="Login with Google"
						onSuccess={ authenticateGoogleToken }
						onFailure={ authenticateGoogleToken }
						cookiePolicy={ 'single_host_origin' }
						className="w-100 text-center d-flex justify-content-center"
					/>

					<Link href="/register">
						<a>
							<p>Not yet registered? Sign up now!</p>
						</a>
					</Link>

				</Form>
			</Container>
		</React.Fragment>
	)
}

 