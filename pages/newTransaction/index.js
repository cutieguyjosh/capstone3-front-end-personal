import React, { useContext, useState, useEffect } from 'react';
import Link from 'next/link';
import Router from 'next/router';
import Head from 'next/head';
import { Container, Col, Row, Form, Button, Card } from 'react-bootstrap';
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2';

export default function index() {

	const [id, setId] = useState("")
	const [description, setDescription] = useState("")
	const [amount, setAmount] = useState("")
	const [transactionType, setTransactionType] = useState("")
	const [categoryName, setCategoryName] = useState("")
	const [option, setOption] = useState()


	useEffect (() => {

		const getCategoryDetails = async () => {


			const options = {
				headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
			}

			const fetchCategories = await fetch('https://aqueous-hamlet-35125.herokuapp.com/api/users/getCategories', options)

			const data = await fetchCategories.json()

			// console.log(data);
			const categoryMap = data.map(categories => {

				if (categories.category_type === transactionType) {

					return (
						<option key={ categories._id } value={ categories.category_name }>
							{ categories.category_name }
						</option>
					)
				} 
			})

			setCategoryName(categoryMap)
		}
			getCategoryDetails()

	}, [transactionType]) 

	function newTransaction(e) {
		e.preventDefault()

		const options = {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				category_name: option,
				transaction_type: transactionType,
				description: description,
				amount: amount
			})
		}

		fetch('https://aqueous-hamlet-35125.herokuapp.com/api/users/transaction', options)
		.then(AppHelper.toJSON)
		.then(data => {
			if (data) {
				Swal.fire("Good job!", "Transaction successfully added", "success")
			} else {
				alertSwal.fire("Uh-oh!", "Please Try again!", "error")
			}

			Router.push('/dashboard')

			setDescription("")
			setAmount("")
			setTransactionType("")
			setCategoryName("")

		})

	}

<Form.Control placeholder="Enter Category name" onChange={(e) => setCategoryName(e.target.value)}/>


	return (
		<React.Fragment>
			<Head>
				<title>Add Transaction</title>
			</Head>
			<Container>
				<h2 className="mt-4">Add Transaction</h2>
				<br />
				<div className="mt-5 pt-4 mb-3">
				<div className="justify-content-center row">
				<div className="col-md-6 col">
				<Link href="/newCategory">
				<Button variant="success" className="mt-2">Add Category</Button>
				</Link>
					<Form onSubmit={e => newTransaction(e)}>

						<Form.Group>
							<Form.Label>Transaction Type:</Form.Label>
							<Form.Control as="select" onChange={(e) => setTransactionType(e.target.value)}>
							<option>Select One:</option>
							<option value="Income">Income</option>
							<option value="Expense">Expense</option>
							</Form.Control>
						</Form.Group>

						<Form.Group>
							<Form.Label>Category Name:</Form.Label>
							<Form.Control as="select" value={ option } onChange={(e) => setOption(e.target.value)}>
							<option></option>
							{ categoryName }
							</Form.Control>
						</Form.Group>

						

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" placeholder="Enter description of transaction" onChange={(e) => setDescription(e.target.value)} required />
						</Form.Group>

						<Form.Group>
							<Form.Label>Amount</Form.Label>
							<Form.Control type="number" placeholder="Enter amount" required onChange={(e) => setAmount(e.target.value)} />
						</Form.Group>

						<Button variant="primary" type="submit" id="submitBtn">Submit</Button>					
					</Form>
				</div>
				</div>
				</div>
			</Container>
		</React.Fragment>
	)

} 