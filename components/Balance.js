import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import { Col, Row, Table } from 'react-bootstrap';
import toNum from '../pages/toNum';

export default function index() {

	const [income, setIncome] = useState()
	const [expense, setExpense] = useState()
	const [totalIncome, setTotalIncome] = useState()
	const [totalExpense, setTotalExpense] = useState()


	useEffect (() => {

		const allStatus = async () => {

			const options = {
				headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
			}

			const balanceStatus = await fetch('https://aqueous-hamlet-35125.herokuapp.com/api/users/details', options)

			const data = await balanceStatus.json()

			let incomeGroup = []
			let expenseGroup = []
			let income = 0
			let expense = 0


			data.transaction.map(computation => {

				if(computation.transaction_type === 'Income') {
					incomeGroup.push(computation.amount)
					income += toNum(computation.amount)
				}
				else if(computation.transaction_type === 'Expense') {
					expenseGroup.push(computation.amount)
					expense += toNum(computation.amount)
				}

			})

			setIncome(incomeGroup)
			setExpense(expenseGroup)
			setTotalIncome(income)
			setTotalExpense(expense)

		}

			allStatus();

	}, [])

	return(
		<Table>
			<thead>
				<tr className="text-center">
					<th>Total Income:</th>
					<th>Total Expense:</th>
					<th>Current Balance:</th>
				</tr>
			</thead>
			<tbody>
				<tr className="text-center">
					<td className="plus">₱{ totalIncome }</td>
					<td className="minus">₱{ totalExpense }</td>
					<td className="strong">₱{ totalIncome - totalExpense }</td>
				</tr>
			</tbody>
		</Table>
	)

}
