import Link from 'next/link';
import { Jumbotron, Row, Col, Button } from 'react-bootstrap';

export default function Banner({data}) {

	const { title, content, destination, label } = data;

	return(
		<Row>
			<Col>
				<Jumbotron>
					<h1>{title}</h1>
					<p>{content}</p>
					<Link href={destination}>
						<a>
							<Button variant="primary">Register Now</Button>
						</a>
					</Link>
					<br />
					<br />
					<Link href='../login'>
						<a>
							<Button variant="primary">Login</Button>
						</a>
					</Link>
				</Jumbotron>
			</Col>
		</Row>
	)

}