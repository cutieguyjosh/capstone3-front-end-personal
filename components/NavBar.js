import React, { useContext } from 'react';
import Link from 'next/link';
import { Navbar, Nav } from 'react-bootstrap';
import UserContext from '../UserContext';


export default function NavBar() {

	const { user } = useContext(UserContext);

	return(
		<Navbar bg="success" expand="md">
			<Navbar.Brand><h6>Budgetarian</h6></Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
				{(user.id !== undefined)
					?
					<React.Fragment>
							<Link href="/dashboard">
								<a className="nav-link" role="button">Dashboard</a>
							</Link>
							<Link href="/newTransaction">
								<a className="nav-link" role="button">Add Transaction</a>
							</Link>
							<Link href="/newCategory">
								<a className="nav-link" role="button">Add Category</a>
							</Link>
							<Link href="/analytics">
								<a className="nav-link" role="button">Analytics</a>
							</Link>
							
							<Link href="/logout">
								<a className="nav-link" role="button">Logout</a>
							</Link>
					</React.Fragment>
					:
					<React.Fragment>
						<Link href="/login">
						 	<a className="nav-link" role="button">Login</a>
							</Link>
						<Link href="/register">
							 <a className="nav-link" role="button">Register</a>
							</Link>
					</React.Fragment>
				}
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}


